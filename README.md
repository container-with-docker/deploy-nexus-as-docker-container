# Deploy-Nexus-as-docker-container


The benefit of provisioning nexus as a container is that it is mush faster, light weighter and comes with an 
already create user - best practice promoting the usage of least preveledge user and avoid the usage of
root user when working with any environments, due to security concern tha it poses.

Workflow
- Creating and configuring a droplet server on DigitalOcean Cloud
- Setting up and running Nexus as a docker container.

```
Technology Used
Docker 
Nexus
DigitalOcean Cloud
Linux
```

```
cd existing_repo
git remote add origin https://gitlab.com/container-with-docker/deploy-nexus-as-docker-container.git
git branch -M main
git push -uf origin main
```

